import QtQuick 2.5
import QtQuick.Layouts 1.2
import QtQuick.Controls.Material 2.0

Text {
    //font.pointSize: theme.defaultFontSize - 2
    Layout.fillWidth: true
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    color: Material.secondaryTextColor
    //
}
